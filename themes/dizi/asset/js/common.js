$(document).ready(function () {

  // media queries
  var mq = [
    window.matchMedia("(max-width: 767px)"),
    window.matchMedia("(min-width: 768px) and (max-width: 1179px)"),
    window.matchMedia("(min-width: 1180px)")
  ];
  
  
  // toggle topics
  $(document).on("click", ".topics-link-js", function (e) {
    $(".topics-js").toggleClass("open");
    e.preventDefault();
  });


  // sliders
  if ($.fn.slick) {
    $(".cover-slider-js").slick({
      dots: true,
      autoplay: false,
      variableWidth: true,
      slidesToScroll: 1,
      rows: 0,
	  infinite: false,
      prevArrow: '<button type="button" class="slick-prev" aria-label="Previous"><i class="icon-slide-prev" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="slick-next" aria-label="Next"><i class="icon-slide-next" aria-hidden="true"></i></button>'
    });
	$(".content-gallery-js").slick({
      autoplay: false,
      fade: true,
      adaptiveHeight: true,
      slidesToScroll: 1,
      rows: 0,
      prevArrow: '<button type="button" class="slick-prev" aria-label="Previous"><i class="icon-slide-prev" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="slick-next" aria-label="Next"><i class="icon-slide-next" aria-hidden="true"></i></button>'
    });
  }
 
	if($('.content__block').length){
		$('.content__block').each(function(){
			if ($(this).html().trim().length == 0){
				$(this).hide();
			}
		});
	}
	
	// cookies toggle
	(function() {
	if (!localStorage.getItem('cookieconsent')) {
		$(document).on("click", ".cookies-link-js", function (e) {
			$(this).closest(".cookies-js").fadeOut(function () {
			$(this).remove();
			});
			e.preventDefault();
			localStorage.setItem('cookieconsent', true);
		});
	}else{
		$('.cookies-js').remove();
	}
	})();

	
	// animation on scroll
	AOS.init({
		once: true
	});


  // sticky
  if ($.fn.sticky) {
    var stickyOptions = {
      responsiveWidth: true,
      zIndex: 5
    };
    $(window).on("resize", function () {
      if (mq[0].matches) {
        $(".sticky-js").unstick();
      } else {
        $(".sticky-js").sticky(stickyOptions);
      }
    }).trigger("resize");
  }
  
  if($('#sharing-buttons').length){
	  if($(".share").length){
		$("#sharing-buttons").appendTo(".share");
	  }else{
		$("#sharing-buttons").remove();
	  }
  }else{
	if($(".share-link").length){
		$(".share-link").remove();
	}
  }
  
    // modal zoom ggallery
  var initPhotoSwipeFromDOM = function (gallerySelector) {

    // parse slide data (url, title, size ...) from DOM elements
    // (children of gallerySelector)
    var parseThumbnailElements = function (el) {
      var thumbElements = el.childNodes,
        numNodes = thumbElements.length,
        items = [],
        figureEl,
        linkEl,
        size,
        item;

      for (var i = 0; i < numNodes; i++) {

        figureEl = thumbElements[i]; // <figure> element

        // include only element nodes
        if (figureEl.nodeType !== 1) {
          continue;
        }

        linkEl = figureEl.children[0]; // <a> element

        size = linkEl.getAttribute('data-size').split('x');

        // create slide object
        item = {
          src: linkEl.getAttribute('href'),
          w: parseInt(size[0], 10),
          h: parseInt(size[1], 10)
        };

        if (figureEl.children.length > 1) {
          // <figcaption> content
          item.title = figureEl.children[1].innerHTML;
        }

        if (linkEl.children.length > 0) {
          // <img> thumbnail element, retrieving thumbnail url
          item.msrc = linkEl.children[0].getAttribute('src');
        }

        item.el = figureEl; // save link to element for getThumbBoundsFn
        items.push(item);
      }

      return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
      return el && (fn(el) ? el : closest(el.parentNode, fn));
    };

    var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
      var pswpElement = document.querySelectorAll('.pswp')[0],
        gallery,
        options,
        items;

      items = parseThumbnailElements(galleryElement);

      // define options (if needed)
      options = {

        // define gallery index (for URL)
        galleryUID: galleryElement.getAttribute('data-pswp-uid'),

        getThumbBoundsFn: function (index) {
          // See Options -> getThumbBoundsFn section of documentation for more info
          var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
            rect = thumbnail.getBoundingClientRect();

          return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
        }

      };

      // PhotoSwipe opened from URL
      if (fromURL) {
        if (options.galleryPIDs) {
          // parse real index when custom PIDs are used
          // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
          for (var j = 0; j < items.length; j++) {
            if (items[j].pid === index) { // jshint ignore:line
              options.index = j;
              break;
            }
          }
        } else {
          // in URL indexes start from 1
          options.index = parseInt(index, 10) - 1;
        }
      } else {
        options.index = parseInt(index, 10);
      }

      // exit if index not found
      if (isNaN(options.index)) {
        return;
      }

      if (disableAnimation) {
        options.showAnimationDuration = 0;
      }

      // Pass data to PhotoSwipe and initialize it
      gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options); // jshint ignore:line
      gallery.init();
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function (e) {
      e = e || window.event;
      e.preventDefault ? e.preventDefault() : e.returnValue = false; // jshint ignore:line

      var eTarget = e.target || e.srcElement;

      // find root element of slide
      var clickedListItem = closest(eTarget, function (el) {
        return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
      });

      if (!clickedListItem) {
        return;
      }

      // find index of clicked item by looping through all child nodes
      // alternatively, you may define index via data- attribute
      var clickedGallery = clickedListItem.parentNode,
        childNodes = clickedListItem.parentNode.childNodes,
        numChildNodes = childNodes.length,
        nodeIndex = 0,
        index;

      for (var i = 0; i < numChildNodes; i++) {
        if (childNodes[i].nodeType !== 1) {
          continue;
        }

        if (childNodes[i] === clickedListItem) {
          index = nodeIndex;
          break;
        }
        nodeIndex++;
      }

      if (index >= 0) {
        // open PhotoSwipe if valid index found
        openPhotoSwipe(index, clickedGallery);
      }
      return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function () {
      var hash = window.location.hash.substring(1),
        params = {};

      if (hash.length < 5) {
        return params;
      }

      var vars = hash.split('&');
      for (var i = 0; i < vars.length; i++) {
        if (!vars[i]) {
          continue;
        }
        var pair = vars[i].split('=');
        if (pair.length < 2) {
          continue;
        }
        params[pair[0]] = pair[1];
      }

      if (params.gid) {
        params.gid = parseInt(params.gid, 10);
      }

      return params;
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll(gallerySelector);

    for (var i = 0, l = galleryElements.length; i < l; i++) {
      galleryElements[i].setAttribute('data-pswp-uid', i + 1);
      galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if (hashData.pid && hashData.gid) {
      openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
    }
  };

  // execute above function
  initPhotoSwipeFromDOM('.imageZoom-js');


  // modal
  if ($.fn.magnificPopup) {

    let windowPos;

    $(".modal-gallery-js").each(function () {
      $(this).magnificPopup({
        delegate: ".content__gallery__panels:not(.slick-cloned) a",
        type: "image",
        tClose: "",
        tLoading: '<i aria-hidden="true" class="icon-spinner"></i>',
        removalDelay: 300,
        mainClass: "mfp-with-zoom",
        closeMarkup: '<button title="%title%" type="button" class="mfp-close"><i aria-hidden="true" class="icon-close"></i></button>',
        gallery: {
          enabled: true,
          tPrev: "",
          tNext: ""
        },
        callbacks: {
          beforeOpen: function () {
            windowPos = $(window).scrollTop();
            $("body").addClass("modal_open");
          },
          afterClose: function () {
            $("body").removeClass("modal_open");
            $(window).scrollTop(windowPos);
          }
        }
      });
    });

  }

  
	// anchor with fixed size header
	var anchorLink = $(window.location.hash);
		if ( anchorLink.length ) {
			$("html, body").animate({scrollTop: anchorLink.offset().top - 90 }, 500);
    }

});
