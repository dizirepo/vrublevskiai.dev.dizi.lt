// including plugins
const
  autoprefixer = require('gulp-autoprefixer'),
  babel = require('gulp-babel'),
  browserSync = require('browser-sync').create(),
  cached = require('gulp-cached'),
  cleanCSS = require('gulp-clean-css'),
  concat = require('gulp-concat'),
  gulp = require('gulp'),
  gulpIf = require('gulp-if'),
  imagemin = require('gulp-imagemin'),
  imageminMozjpeg = require('imagemin-mozjpeg'),
  imageminPngquant = require('imagemin-pngquant'),
  imageminZopfli = require('imagemin-zopfli'),
  less = require('gulp-less'),
  plumber = require('gulp-plumber'),
  remember = require('gulp-remember'),
  rename = require('gulp-rename'),
  sourcemaps = require('gulp-sourcemaps'),
  uglify = require('gulp-uglify-es').default;


const
  cache = {
    js: 'cache-js',
    jsMin: 'cache-min-js'
  },
  src = {
    images: ['img/**/*.{gif,png,jpg,jpeg,svg}'],
    css: 'css/style.less',
    js: ['js/jquery/**/*.js', 'js/plugins/**/*.js', 'js/common.js']
  },
  onError = function (err) {
    console.log('\007' + err.message);
    this.emit('end');
  };

// optimize images
gulp.task('imagemin', () => {
  return gulp.src(src.images)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(imagemin([
      imageminPngquant({
        speed: 1,
        quality: 100
      }),
      imageminZopfli({
        more: true
      }),
      imagemin.gifsicle({
        interlaced: true,
        optimizationLevel: 3
      }),
      imagemin.svgo({
        plugins: [{
          removeViewBox: false
        }, {
          convertStyleToAttrs: false
        }, {
          removeUnknownsAndDefaults: false
        }]
      }),
      imageminMozjpeg({
        sample: ['1x1'],
        quantTable: 2,
        trellis: false,
        progressive: true,
        quality: 90
      })
    ]))
    .pipe(gulp.dest('img'));
});

// pack-css
gulp.task('pack-css', () => {
  return gulp.src(src.css)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(autoprefixer({
      browsers: ['last 1 version'],
      cascade: false
    }))
    .pipe(gulp.dest('css/'))
    .pipe(cleanCSS({
      level: {
        1: {
          specialComments: 0
        }
      }
    }))
    .pipe(rename('style.min.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('css/'))
    .pipe(browserSync.stream());
});

// pack-js
gulp.task('pack-js', () => {
  gulp.src(src.js)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(cached(cache.js))
    .pipe(gulpIf(src.js[src.js.length - 1].split('/').pop(), babel({
      compact: false,
      presets: ['env']
    })))
    .pipe(remember(cache.js))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('js/'));

  return gulp.src(src.js)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(cached(cache.jsMin))
    .pipe(sourcemaps.init())
    .pipe(gulpIf(src.js[src.js.length - 1].split('/').pop(), babel({
      compact: false,
      presets: ['env']
    })))
    .pipe(gulpIf(file => !(file.path.includes('.min.js')), uglify()))
    .pipe(remember(cache.jsMin))
    .pipe(concat('main.min.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('js/'))
    .pipe(browserSync.stream());
});

// browsersync + watch edited files + remove from cache deleted files
gulp.task('watch', () => {
  browserSync.init({
    server: {
      baseDir: './',
      directory: true
    },
    notify: false,
    port: 8080,
    open: 'external',
    ghostMode: false
  });

  gulp.watch(['css/**/*.css', 'css/**/*.less', '!css/style.css', '!css/style.min.css'], ['pack-css']);
  gulp.watch(['js/**/*.js', '!js/main.js', '!js/main.min.js'], ['pack-js']).on('change', function (event) {
    if (event.type === 'deleted') {
      delete cached.caches[cache.js][event.path];
      delete cached.caches[cache.jsMin][event.path];
      remember.forget(cache.js, event.path);
      remember.forget(cache.jsMin, event.path);
    }
  });
  gulp.watch(['img/**/*.{gif,png,jpg,jpeg,svg}', '*.html']).on('change', browserSync.reload);
});

// default task
gulp.task('default', ['watch']);
