<?php
namespace MultiFiles;

use MultiFiles\Form\ConfigForm;
use Omeka\Module\AbstractModule;
use Omeka\Entity\Media;
use Omeka\File\Validator;
use Omeka\File\Uploader;
use Zend\EventManager\Event;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\Mvc\Controller\AbstractController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Renderer\PhpRenderer;

class Module extends AbstractModule
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
	
	public function attachListeners(SharedEventManagerInterface $sharedEventManager)
    {
        $sharedEventManager->attach(
            \Omeka\Api\Adapter\ItemAdapter::class,
            'api.create.post',
            [$this, 'afterSaveItem'],
            100
        );
		$sharedEventManager->attach(
            \Omeka\Api\Adapter\ItemAdapter::class,
            'api.update.post',
            [$this, 'afterSaveItem'],
            100
        );
	}
	
	/**
     * Manages folders for attached files of items.
     */
    public function afterSaveItem(Event $event)
    {
		$services = $this->getServiceLocator();
		$apiManager = $services->get('Omeka\ApiManager');
        $item = $event->getParam('response')->getContent();
		$data = $event->getParam('request')->getContent();
        $files = $event->getParam('request')->getFileData();
		foreach($data["o:media"] as $omedia){
			$index = (empty($omedia['file_index'])) ? 0 : $omedia['file_index'];
			if(!empty($omedia["o:ingester"]) && $omedia["o:ingester"]=='multifiles' && is_array($files['file'][$index])){
				foreach($files['file'][$index] as $fileData){
					$this->afterSaveMedia($item, $fileData, $apiManager);
				}
			}
		}
		foreach($item->getMedia()->getSnapshot() as $id => $info){
			if($info->getIngester()=='multifiles'){
				if($id==0){
					$id=$info->getId();
				}
				$apiManager->delete('media', $id);
			}
		}
	}

    /**
     * Set medias at the right place.
     *
     * @param Media $media
     */
    protected function afterSaveMedia($item, $file, $apiManager)
    {
        $fileIndex=0;
		$data=[
			"o:ingester" => "upload",
			"file_index" => $fileIndex,
			"o:item" => [
				"o:id" => $item->getId()
			],
		];
		$fileData = [
			'file'=>[
				$fileIndex => $file,
			],
		];
		$response = $apiManager->create('media', $data, $fileData);
    }
}
