<?php
namespace MultiFiles;

return [
    'media_ingesters' => [
        'factories' => [
            'multifiles' => Service\MediaIngesterMultiFilesFactory::class,
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
                'text_domain' => null,
            ],
        ],
    ],
];
