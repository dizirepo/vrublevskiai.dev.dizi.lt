<?php
namespace MultiFiles\Media\Ingester;

use Omeka\Api\Request;
use Omeka\Entity\Media;
use Omeka\Media\Ingester\IngesterInterface;
use Omeka\Stdlib\ErrorStore;
use Omeka\File\Validator;
use Omeka\File\Uploader;
use Omeka\Api\Manager as ApiManager;
use Zend\Form\Element\File;
use Zend\View\Renderer\PhpRenderer;


class MultiFiles implements IngesterInterface
{

    public function __construct(Validator $validator, Uploader $uploader)
    {
        $this->validator = $validator;
        $this->uploader = $uploader;
    }

    public function getLabel()
    {
        return 'Įkelti daugiau failų iš karto'; // @translate
    }

    public function getRenderer()
    {
        return 'file';
    }

    /**
     * Ingest from a URL.
     *
     * Accepts the following non-prefixed keys:
     *
     * + ingest_filename: (required) The filename to ingest.
     * + store_original: (optional, default true) Store the original file?
     *
     * {@inheritDoc}
     */
    public function ingest(Media $media, Request $request, ErrorStore $errorStore)
    {
		return;
    }
	

    public function form(PhpRenderer $view, array $options = [])
    {
        $fileInput = new File('file[__index__]');
        $fileInput->setOptions([
            'label' => 'Upload file', // @translate
            'info' => $view->uploadLimit(),
        ]);
        $fileInput->setAttributes([
            'id' => 'media-file-input-__index__',
            'required' => true,
			'multiple' => true,
        ]);
        $field = $view->formRow($fileInput);
        return $field . '<input type="hidden" name="o:media[__index__][file_index]" value="__index__">';
    }
}


