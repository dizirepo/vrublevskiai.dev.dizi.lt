<?php
namespace MultiFiles\Service;

use MultiFiles\Media\Ingester\MultiFiles;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class MediaIngesterMultiFilesFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $services, $requestedName, array $options = null)
    {
        return new MultiFiles(
			$services->get('Omeka\File\Validator'),
            $services->get('Omeka\File\Uploader')
		);
    }
}
