<?php

namespace ArchiveFolder;

use Zend\EventManager\SharedEventManagerInterface;
use Zend\Mvc\Controller\AbstractController;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Renderer\PhpRenderer;
use Omeka\Module\AbstractModule;
use ArchiveFolder\Importer;

/**
 * Archive Folder
 *
 * Automatically build an archive from files and metadata of a local or a
 * distant folder.
 *
 * @copyright Copyright Daniel Berthereau, 2015
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 * @package ArchiveFolder
 */

/**
 * The Archive Folder plugin.
 */
class Module extends AbstractModule
{
    /**
     * @var array This plugin's options.
     */
    protected $settings = array(
        'archive_folder_static_dir' => 'documents',
        'archive_folder_processor' => '',
        'archive_folder_short_dispatcher' => null,
        'archive_folder_slow_process' => 0,
        'archive_folder_memory_limit' => null,
        // With roles, in particular if Guest User is installed.
        'archive_folder_allow_roles' => 'a:1:{i:0;s:5:"super";}',
        // Options for a new archive folder.
        'archive_folder_identifier_field' => 'none', //Importer::DEFAULT_IDFIELD,
        'archive_folder_action' => 'update else create', //Importer::DEFAULT_ACTION,
    );

    /**
     * Installs the plugin.
     */
    public function install(ServiceLocatorInterface $serviceLocator)
    {
        $connection = $serviceLocator->get('Omeka\Connection');
        $settings = $serviceLocator->get('Omeka\Settings');

        // Create a table for folders.
        $sql = "
            CREATE TABLE IF NOT EXISTS `archivefolder_folder` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                `parameters` text collate utf8_unicode_ci NOT NULL,
                `status` enum('added', 'reset', 'queued', 'progress', 'paused', 'stopped', 'killed', 'completed', 'deleted', 'error') NOT NULL,
                `messages` longtext COLLATE utf8_unicode_ci NOT NULL,
                `owner_id` int(11) NOT NULL,
                `added` datetime NOT NULL,
                `modified` datetime NOT NULL,
                PRIMARY KEY (`id`),
                INDEX `uri` (`uri`),
                UNIQUE `identifier` (`identifier`),
                INDEX `modified` (`modified`),
                FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ";
        $connection->exec($sql);

        // Create a table to list processed records.
        $sql = "
            CREATE TABLE IF NOT EXISTS `archivefolder_record` (
                `id` int(11) unsigned NOT NULL auto_increment,
                `folder_id` int(11) unsigned NOT NULL,
                `index` int(11) unsigned NOT NULL,
                `record_type` varchar(50) collate utf8_unicode_ci NOT NULL,
                `record_id` int(11) unsigned NOT NULL,
                PRIMARY KEY  (`id`),
                INDEX (`folder_id`),
                INDEX `folder_id_index` (`folder_id`, `index`),
                INDEX `record_type_record_id` (`record_type`, `record_id`),
                FOREIGN KEY (`folder_id`) REFERENCES `archivefolder_folder` (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ";
        $connection->exec($sql);

        foreach ($this->settings as $name => $value) {
            $settings->set($name, $value);
        }

        // Check if there is a folder for the static repositories files, else
        // create one and protect it.
        $filesDir = OMEKA_PATH . DIRECTORY_SEPARATOR . 'files';
        $staticDir = $filesDir . DIRECTORY_SEPARATOR . $settings->get('archive_folder_static_dir');
        if (!file_exists($staticDir)) {
            mkdir($staticDir, 0755, true);
            copy($filesDir . DIRECTORY_SEPARATOR . 'index.html',
                $staticDir . DIRECTORY_SEPARATOR . 'index.html');
        }
    }

    /**
     * Uninstalls the plugin.
     */
    public function uninstall(ServiceLocatorInterface $serviceLocator)
    {
        $connection = $serviceLocator->get('Omeka\Connection');
        $settings = $serviceLocator->get('Omeka\Settings');

        $sql = "DROP TABLE IF EXISTS `archivefolder_folder`";
        $connection->exec($sql);
        $sql = "DROP TABLE IF EXISTS `archivefolder_record`";
        $connection->exec($sql);

        foreach ($this->settings as $name => $value) {
            $settings->delete($name);
        }
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        parent::onBootstrap($event);

        $this->defineAcl();
    }

    /**
     * Define the plugin's access control list.
     */
    protected function defineAcl()
    {
        $serviceLocator = $this->getServiceLocator();
        $acl = $serviceLocator->get('Omeka\Acl');
        $settings = $serviceLocator->get('Omeka\Settings');

        $resource = 'ArchiveFolder\Controller\Index';

        // Hack to disable CRUD actions.
        $acl->deny(null, $resource, array('show', 'add', 'edit', 'delete'));
        $acl->deny(null, $resource);

        $roles = $acl->getRoles();

        // Check that all the roles exist, in case a plugin-added role has
        // been removed (e.g. GuestUser).
        $allowRoles = unserialize($settings->get('archive_folder_allow_roles')) ?: [];
        $allowRoles = array_intersect($roles, $allowRoles);
        if ($allowRoles) {
            $acl->allow($allowRoles, $resource);
        }

        $denyRoles = array_diff($roles, $allowRoles);
        if ($denyRoles) {
            $acl->deny($denyRoles, $resource);
        }
    }

    /**
     * Shows plugin configuration page.
     */
    public function getConfigForm(PhpRenderer $renderer)
    {
        $acl = $this->getServiceLocator()->get('Omeka\Acl');
        $roles = $acl->getRoleLabels();
        return $renderer->render('archive-folder/config-form', ['roles' => $roles]);
    }

    /**
     * Handle a submitted config form.
     */
    public function handleConfigForm(AbstractController $controller)
    {
        $settings = $this->getServiceLocator()->get('Omeka\Settings');

        $params = $controller->getRequest()->getPost();
        if (isset($params['archive_folder_allow_roles'])) {
            $params['archive_folder_allow_roles'] = serialize($params['archive_folder_allow_roles']);
        }

        foreach ($params as $name => $value) {
            if (array_key_exists($name, $this->settings)) {
                $settings->set($name, $value);
            }
        }
    }

    public function attachListeners(SharedEventManagerInterface $sharedEventManager)
    {
        $sharedEventManager->attach('ArchiveFolder', 'mappings',
            [$this, 'archiveFolderMappings']);
        $sharedEventManager->attach('ArchiveFolder', 'archive_folder_ingesters',
            [$this, 'archiveFolderIngesters']);
    }

    /**
     * Add the mappings to convert metadata files into Omeka elements.
     *
     * @return array Filtered mappings array.
    */
    public function archiveFolderMappings()
    {
        $translator = $this->getServiceLocator()->get('MvcTranslator');

        // Available mappings in the plugin at first place to keep order.
        $mappings = [
            'doc' => [
                'class' => 'ArchiveFolder\Mapping\Document',
                'description' => $translator->translate('Documents xml (simple format that manages all features of Omeka)'),
            ],
            'text' => [
                'class' => 'ArchiveFolder\Mapping\Text',
                'description' => $translator->translate('Text (extension: ".metadata.txt")'),
            ],
            'json' => [
                'class' => 'ArchiveFolder\Mapping\Json',
                'description' => $translator->translate('Json (extension: ".json")'),
            ],
            'odt' => [
                'class' => 'ArchiveFolder\Mapping\Odt',
                'description' => $translator->translate('Open Document Text (extension: ".odt")'),
            ],
            'ods' => [
                'class' => 'ArchiveFolder\Mapping\Ods',
                'description' => $translator->translate('Open Document Spreadsheet (extension: ".ods")'),
            ],
            'mets' => [
                'class' => 'ArchiveFolder\Mapping\Mets',
                'description' => $translator->translate('METS xml (with a profile compliant with Dublin Core)'),
            ],
        ];

        return $mappings;
    }

    /**
     * Add the ingesters for associated files that are available.
     *
     * @internal The prefix is a value to allow multiple ways to format data.
     *
     * @return array Filtered Ingesters array.
    */
    public function archiveFolderIngesters()
    {
        $translator = $this->getServiceLocator()->get('MvcTranslator');

        // Available ingesters in the plugin at first place to keep order.
        $ingesters = [
            'alto' => [
                'prefix' => 'alto',
                'class' => 'ArchiveFolder\Ingester\Alto',
                'description' => $translator->translate('Alto xml files for OCR'),
            ],
        ];

        return $ingesters;
    }
}
