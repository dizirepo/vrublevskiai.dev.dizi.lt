<?php
return [
    'controllers' => [
        'invokables' => [
            'ArchiveFolder\Controller\Index' => 'ArchiveFolder\Controller\IndexController',
            'ArchiveFolder\Controller\Ajax' => 'ArchiveFolder\Controller\AjaxController',
        ],
    ],
    'api_adapters' => [
        'invokables' => [
            'archivefolder_folders' => 'ArchiveFolder\Api\Adapter\FolderAdapter',
        ],
    ],
    'entity_manager' => [
        'mapping_classes_paths' => [
            __DIR__ . '/../src/Entity',
        ],
    ],
    'validators' => [
        'invokables' => [
            'ArchiveFolderUri' => 'ArchiveFolder\Validator\ArchiveFolderUri',
            'ArchiveFolderExtraParams' => 'ArchiveFolder\Validator\ArchiveFolderExtraParams',
        ],
    ],
    'router' => [
        'routes' => [
            'archivefolder_index' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/admin/archive-folder/:action',
                    'defaults' => [
                        '__NAMESPACE__' => 'ArchiveFolder\Controller',
                        '__ADMIN__' => true,
                        'controller' => 'Index',
                        'action' => 'browse',
                    ],
                ],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label' => 'Archive Folders',
                'route' => 'archivefolder_index',
                'resource' => 'ArchiveFolder\Controller\Index',
                'privilege' => 'browse',
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
                'text_domain' => null,
            ],
        ],
    ],
    'archive_folder' => [
        'local_folders' => [
            'allow' => true,
            'check_realpath' => false,
            'base_path' => '/home/julian/omeka-s/files',
        ],
    ],
];
