<?php
namespace ArchiveFolder\Entity;

use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Omeka\Entity\AbstractEntity;
use Omeka\Entity\User;

/**
 * @Entity
 * @Table(name="archivefolder_folder")
 * @HasLifecycleCallbacks
 */
class Folder extends AbstractEntity
{
    const STATUS_ADDED = 'added';
    const STATUS_RESET = 'reset';
    const STATUS_QUEUED = 'queued';
    const STATUS_PROGRESS = 'progress';
    const STATUS_PAUSED = 'paused';
    const STATUS_STOPPED = 'stopped';
    const STATUS_KILLED = 'killed';
    const STATUS_COMPLETED = 'completed';
    const STATUS_DELETED = 'deleted';
    const STATUS_ERROR = 'error';
    /**#@-*/

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column
     */
    protected $uri;

    /**
     * @Column
     */
    protected $identifier;

    /**
     * @Column(type="array")
     */
    protected $parameters;

    /**
     * @Column
     */
    protected $status;

    /**
     * @Column(type="array")
     */
    protected $messages;

    /**
     * @ManyToOne(targetEntity="Omeka\Entity\User")
     * @JoinColumn(nullable=false)
     */
    protected $owner;


    /**
     * @Column(type="datetime")
     */
    protected $added;

    /**
     * @Column(type="datetime")
     */
    protected $modified;

    public function getId()
    {
        return $this->id;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function setAdded(DateTime $added)
    {
        $this->added = $added;
    }

    public function getAdded()
    {
        return $this->started;
    }

    public function setModified(DateTime $modified)
    {
        $this->modified = $modified;
    }

    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @PrePersist
     */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $this->added = $this->modified = new DateTime('now');
    }

    /**
     * @PreUpdate
     */
    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->modified = new DateTime('now');
    }
}
