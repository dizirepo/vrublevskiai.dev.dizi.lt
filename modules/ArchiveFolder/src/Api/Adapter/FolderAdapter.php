<?php
namespace ArchiveFolder\Api\Adapter;

use Omeka\Api\Adapter\AbstractEntityAdapter;
use Omeka\Api\Request;
use Omeka\Entity\EntityInterface;
use Omeka\Stdlib\ErrorStore;

/**
 * Module adapter.
 */
class FolderAdapter extends AbstractEntityAdapter
{
    protected $sortFields = [
        'id' => 'id',
        'identifier' => 'identifier',
        'uri' => 'uri',
        'modified' => 'modified',
    ];

    /**
     * {@inheritDoc}
     */
    public function getResourceName()
    {
        return 'archivefolder_adapters';
    }

    /**
     * {@inheritDoc}
     */
    public function getRepresentationClass()
    {
        return 'ArchiveFolder\Api\Representation\FolderRepresentation';
    }

    /**
     * {@inheritDoc}
     */
    public function getEntityClass()
    {
        return 'ArchiveFolder\Entity\Folder';
    }

    /**
     * {@inheritDoc}
     */
    public function hydrate(Request $request, EntityInterface $entity,
        ErrorStore $errorStore)
    {
        $parameters = [];

        $this->hydrateOwner($request, $entity);

        if ($this->shouldHydrate($request, 'uri')) {
            $uri = $request->getValue('uri');
            if (parse_url($url, PHP_URL_HOST)) {
                $repositoryIdentifierBase = parse_url($uri, PHP_URL_PATH);
            } else {
                $repositoryIdentifierBase = $uri;
            }
            $entity->setUri($uri);
        }

        if ($repositoryIdentifierBase) {
            $repositoryIdentifierBase .= '-' . date('Ymd-His') . '-' . rtrim(strtok(substr(microtime(), 2), ' '), '0');
            $parameters['repository_identifier'] = $this->_keepAlphanumericOnly($repositoryIdentifierBase);
            $entity->setIdentifier($parameters['repository_identifier']);
        }

        if ($this->shouldHydrate($request, 'unreferenced_files')) {
            $parameters['unreferenced_files'] = $request->getValue('unreferenced_files');
        }
        if ($this->shouldHydrate($request, 'exclude_extensions')) {
            $parameters['exclude_extensions'] = $request->getValue('exclude_extensions');
        }
        if ($this->shouldHydrate($request, 'element_delimiter')) {
            $parameters['element_delimiter'] = $request->getValue('element_delimiter');
        }
        if ($this->shouldHydrate($request, 'empty_value')) {
            $parameters['empty_value'] = $request->getValue('empty_value');
        }
        if ($this->shouldHydrate($request, 'extra_parameters')) {
            $parameters['extra_parameters'] = $request->getValue('extra_parameters');
        }
        if ($this->shouldHydrate($request, 'records_for_files')) {
            $parameters['records_for_files'] = $request->getValue('records_for_files');
        }
        if ($this->shouldHydrate($request, 'item_type_name')) {
            $parameters['item_type_name'] = $request->getValue('item_type_name');
        }
        if ($this->shouldHydrate($request, 'identifier_field')) {
            $parameters['identifier_field'] = $request->getValue('identifier_field');
        }
        if ($this->shouldHydrate($request, 'action')) {
            $parameters['action'] = $request->getValue('action');
        }

        // Set default parameters if needed.
        $defaults = array(
            'unreferenced_files' => 'by_file',
            'exclude_extensions' => '',
            'element_delimiter' => \ArchiveFolder\Mapping\Table::DEFAULT_ELEMENT_DELIMITER,
            'empty_value' => \ArchiveFolder\Mapping\Table::DEFAULT_EMPTY_VALUE,
            'extra_parameters' => [],
            'records_for_files' => true,
            'item_type_name' => '',
            'identifier_field' => \ArchiveFolder\Importer::DEFAULT_IDFIELD,
            'action' => \ArchiveFolder\Importer::DEFAULT_ACTION,
        );
        $parameters = array_merge($defaults, $parameters);

        if ($this->shouldHydrate($request, 'item_type_id')) {
            $parameters['item_type_id'] = $request->getValue('item_type_id');
        }
        if ($this->shouldHydrate($request, 'extra_parameters')) {
            $parameters['extra_parameters'] = $this->_getExtraParameters($request->getValue('extra_parameters'));
        }

        if (empty($parameters['unreferenced_files'])) {
            $parameters['unreferenced_files'] = $defaults['unreferenced_files'];
        }

        $entity->setParameters($parameters);

        if ($this->shouldHydrate($request, 'status')) {
            $status = $request->getValue('status');
            if (empty($status)) {
                $status = 'added';
            }
            $entity->setStatus($status);
        }

        if ($this->shouldHydrate($request, 'messages')) {
            $messages = $request->getValue('messages');
            if (!isset($messages)) {
                $messages = [];
            }
            $entity->setMessages($messages);
        }
    }

    protected function _keepAlphanumericOnly($string)
    {
        return preg_replace('/[^a-zA-Z0-9\-_\.]/', '', $string);
    }

    protected function _getExtraParameters($extraParameters)
    {
        if (is_array($extraParameters)) {
            return $extraParameters;
        }

        $parameters = array();

        $parametersAdded = array_values(array_filter(array_map('trim', explode(PHP_EOL, $extraParameters))));
        foreach ($parametersAdded as $parameterAdded) {
            list($paramName, $paramValue) = explode('=', $parameterAdded);
            $parameters[trim($paramName)] = trim($paramValue);
        }

        return $parameters;
    }
}
