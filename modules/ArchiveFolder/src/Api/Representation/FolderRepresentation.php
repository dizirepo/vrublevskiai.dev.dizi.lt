<?php

namespace ArchiveFolder\Api\Representation;

use Omeka\Api\Representation\AbstractEntityRepresentation;

class FolderRepresentation extends AbstractEntityRepresentation
{
    /**
     * {@inheritDoc}
     */
    public function getJsonLdType()
    {
        return 'o:ArchiveFolderFolder';
    }

    public function getJsonLd()
    {
        $owner = null;
        if ($this->owner()) {
            $owner = $this->owner()->getReference();
        }

        $added = [
            '@value' => $this->getDateTime($this->added()),
            '@type' => 'http://www.w3.org/2001/XMLSchema#dateTime',
        ];
        $modified = null;
        if ($this->modified()) {
            $modified = [
               '@value' => $this->getDateTime($this->modified()),
               '@type' => 'http://www.w3.org/2001/XMLSchema#dateTime',
            ];
        }

        return [
            'o:uri' => $this->uri(),
            'o:identifier' => $this->identifier(),
            'o:parameters' => $this->parameters(),
            'o:status' => $this->status(),
            'o:messages' => $this->messages(),
            'o:owner' => $owner,
            'o:added' => $added,
            'o:modified' => $modified,
        ];
    }

    public function uri()
    {
        return $this->resource->getUri();
    }

    public function identifier()
    {
        return $this->resource->getIdentifier();
    }

    public function parameters()
    {
        return $this->resource->getParameters();
    }

    public function status()
    {
        return $this->resource->getStatus();
    }

    public function messages()
    {
        return $this->resource->getMessages();
    }

    public function owner()
    {
        return $this->resource->getOwner();
    }

    public function added()
    {
        return $this->resource->getAdded();
    }

    public function modified()
    {
        return $this->resource->getModified();
    }
}
