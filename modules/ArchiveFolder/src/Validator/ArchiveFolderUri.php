<?php

namespace ArchiveFolder\Validator;

use Zend\Validator\Uri as ValidatorUri;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class ArchiveFolderUri extends ValidatorUri implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const SECURITY = 'security';

    public function __construct($options = []) {
        $this->messageTemplates[self::SECURITY] = 'The URI does not match security settings';

        parent::__construct($options);
    }

    /**
     * Callback to check extra-parameters.
     *
     * @param string $value The value to check.
     * @return boolean
     */
    public function isValid($uri)
    {
        $scheme = parse_url($uri, PHP_URL_SCHEME);
        if (in_array($scheme, ['http', 'https', 'ftp', 'sftp'])) {
            return parent::isValid($uri);
        }

        // Unknown or unmanaged scheme.
        if ($scheme != 'file' && $uri[0] != '/') {
            $this->error(self::SECURITY);
            return false;
        }

        // Check the security setting.
        $serviceLocator = $this->getServiceLocator()->getServiceLocator();
        $config = $serviceLocator->get('Config');
        if (!$config['archive_folder']['local_folders']['allow']) {
            $this->error(self::SECURITY);
            return false;
        }

        // Check the base path.
        $basepath = $config['archive_folder']['local_folders']['base_path'];
        $realpath = realpath($basepath);
        if ($basepath !== $realpath || strlen($realpath) <= 2) {
            $this->error(self::SECURITY);
            return false;
        }

        // Check the uri.
        if ($config['local_folders']['check_realpath']) {
            if (strpos(realpath($uri), $realpath) !== 0
                || !in_array(substr($uri, strlen($realpath), 1), array('', '/')))
            {
                $this->error(self::SECURITY);
                return false;
            }
        }

        // The uri is allowed.
        return true;
    }
}
