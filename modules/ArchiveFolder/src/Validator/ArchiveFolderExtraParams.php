<?php

namespace ArchiveFolder\Validator;

use Zend\Validator\AbstractValidator;

class ArchiveFolderExtraParams extends AbstractValidator
{
    const BAD_FORMAT = 'badFormat';

    protected $messageTemplates = [
        self::BAD_FORMAT => 'Format must be name=value',
    ];

    /**
     * Callback to check extra-parameters.
     *
     * @param string $value The value to check.
     * @return boolean
     */
    public function isValid($value)
    {
        $value = trim($value);
        if (empty($value)) {
            return true;
        }

        $parametersAdded = array_values(array_filter(array_map('trim', explode(PHP_EOL, $value))));
        $parameterNameErrors = array();
        $parameterValueErrors = array();
        foreach ($parametersAdded as $parameterAdded) {
            if (strpos($parameterAdded, '=') === FALSE) {
                $this->error(self::BAD_FORMAT);
                return false;
            }

            list($paramName, $paramValue) = explode('=', $parameterAdded);
            $paramName = trim($paramName);
            if ($paramName == '') {
                $this->error(self::BAD_FORMAT);
                return false;
            }
        }

        return true;
    }
}
