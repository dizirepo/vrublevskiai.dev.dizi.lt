<?php

namespace ArchiveFolder\Form;

use Omeka\Form\AbstractForm;

class AddForm extends AbstractForm
{
    public function buildForm()
    {
        $serviceLocator = $this->getServiceLocator();
        $config = $serviceLocator->get('Config');
        $translator = $serviceLocator->get('MvcTranslator');
        $api = $serviceLocator->get('Omeka\ApiManager');
        $settings = $serviceLocator->get('Omeka\Settings');
        $sharedEventManager = $serviceLocator->get('EventManager');

        $allowLocalPaths = (bool)$config['archive_folder']['local_folders']['allow'];

        $this->setAttribute('id', 'archive-folder');

        $validatorManager = $serviceLocator->get('ValidatorManager');
        $validatorChain = new \Zend\Validator\ValidatorChain();
        $validatorChain->setPluginManager($validatorManager);
        $inputFilter = $this->getInputFilter();
        $inputFilter->getFactory()->setDefaultValidatorChain($validatorChain);

        $infos = [
            $translator->translate('The base url or path of the folder to expose.'),
            $translator->translate('If url, the server should allow directory listing.'),
        ];
        if ($allowLocalPaths) {
            $info[] = $translator->translate('Local paths are forbidden by the administrator.');
        }
        $this->add([
            'name' => 'uri',
            'type' => 'Text',
            'options' => [
                'label' => $translator->translate('Base URI'),
                'info' => implode(' ', $infos),
            ],
            'attributes' => [
                'required' => true,
            ]
        ]);
        $inputFilter->add([
            'name' => 'uri',
            'required' => 'true',
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
            'validators' => [
                ['name' => 'NotEmpty'],
                ['name' => 'ArchiveFolderUri'],
            ],
        ]);


        $mappings = $this->getMappings();
        $info = sprintf(
            $translator->translate('This option indicates what to do with files, maybe all of them, that are not referenced inside metadata files ("%s").'),
            implode('", "', array_keys($mappings))
        );
        $this->add([
            'type' => 'Radio',
            'name' => 'unreferenced_files',
            'options' => [
                'label' => $translator->translate('Unreferenced Files'),
                'info' => $info,
                'value_options' => [
                    'by_file' => $translator->translate('One item by file'),
                    'by_directory' => $translator->translate('One item by directory'),
                    'skip' => $translator->translate("Skip"),
                ],
            ],
            'attributes' => [
                'value' => 'by_file',
            ],
        ]);

        $this->add([
            'type' => 'Hidden',
            'name' => 'records_for_files',
            'attributes' => [
                'value' => true,
            ],
        ]);

        $info = $translator->translate('A black-list of extensions (normal or double) to exclude from the source, separated by a space or a comma and without the initial dot.')
            . ' '
            . $translator->translate('The white-list is the Omeka one, defined in the security page.');
        $this->add([
            'type' => 'Text',
            'name' => 'exclude_extensions',
            'options' => [
                'label' => $translator->translate('File extensions to exclude'),
                'info' => $info,
            ],
        ]);

        $values = [
            '' => $translator->translate('No default type'),
            'default' => $translator->translate('Default type according to first file of each item'),
        ];
        $resource_classes = $api->search('resource_classes', ['vocabulary_prefix' => 'dctype'])->getContent();
        foreach ($resource_classes as $resource_class) {
            $values[$resource_class->id()] = $resource_class->label();
        }
        $this->add([
            'type' => 'Select',
            'name' => 'resource_class_id',
            'options' => [
                'label' => $translator->translate('Default item type'),
                'info' => $translator->translate('Set the item type during import as Omeka S item class and Dublin Core Type.')
                    . ' ' . $translator->translate('For the second option (type of the first file), it can be:')
                    . ' ' . $translator->translate('"Still image" for an item with a single Image, "Text" for an item with multiple image or a pdf, '
                    . '"Sound" for an audio file, "Moving Image" for a video file and none in all other cases.'),
                'value_options' => $values,
            ],
        ]);

        $this->add([
            'type' => 'Checkbox',
            'name' => 'add_relations',
            'options' => [
                'label' => $translator->translate('Add unique identifiers'),
                'info' => $translator->translate('To add unique identifiers allows to link items and files easily and independantly from Omeka.')
                    . ' ' . $translator->translate('Added identifiers are absolute urls.')
                    . ' ' . $translator->translate("This option is only useful when there aren't such identifiers."),
            ],
        ]);

        $this->add([
            'type' => 'Text',
            'name' => 'element_delimiter',
            'options' => [
                'label' => $translator->translate('Table/Spreadsheet element separator'),
                'info' => $translator->translate('If metadata are available in a table (as Open Document Spreadsheet ods), multiple elements can be set within one cell for the same field.')
                    . ' ' . $translator->translate('This character or this string, for example the pipe "|", can be used to delimite them.')
                    . ' ' . $translator->translate('If the delimiter is empty, then the whole text will be used.')
                    . ' ' . $translator->translate('Anyway, multiple columns can be mapped to the same element and multiple rows can manage multiple values for the same field of the same record.'),
            ],
            'attributes' => [
                'value' => \ArchiveFolder\Mapping\Table::DEFAULT_ELEMENT_DELIMITER,
            ],
        ]);

        $this->add([
            'type' => 'Text',
            'name' => 'empty_value',
            'options' => [
                'label' => $translator->translate('Table/Spreadsheet empty value'),
                'info' => $translator->translate('If metadata are available in a table (as Open Document Spreadsheet ods), an empty cell can be an empty value or no value.')
                    . ' ' . $translator->translate('To distinct these two cases, an empty value can be replaced by this string (case sensitive).'),
            ],
            'attributes' => [
                'value' => \ArchiveFolder\Mapping\Table::DEFAULT_EMPTY_VALUE,
            ],
        ]);

        $this->add([
            'type' => 'Textarea',
            'name' => 'extra_parameters',
            'options' => [
                'label' => $translator->translate('Add specific parameters'),
                'info' => $translator->translate('Some formats require specific parameters, for example to be used in the xsl sheets.')
                    . ' ' . $translator->translate('You can specify them here, one by line.'),
            ],
            'attributes' => [
                'rows' => 5,
                'placeholder' => $translator->translate('parameter_1_name = parameter 1 value'),
            ],
        ]);
        $inputFilter->add([
            'name' => 'extra_parameters',
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'ArchiveFolderExtraParams'],
            ],
        ]);

        $identifierField = $settings->get('archive_folder_identifier_field');
        $values = [
            \ArchiveFolder\Importer::IDFIELD_NONE => $translator->translate('No default identifier field'),
            \ArchiveFolder\Importer::IDFIELD_INTERNAL_ID => $translator->translate('Internal id'),
        ];
        $properties = $api->search('properties')->getContent();
        foreach ($properties as $property) {
            $values[$property->id()] = $property->term();
        }
        $this->add([
            'type' => 'Select',
            'name' => 'identifier_field',
            'options' => [
                'label' => $translator->translate('Identifier field'),
                'info' => $translator->translate('The identifier field is used to simplify the update of records.')
                    . ' ' . $translator->translate('This is the link between the files in the folder and the records in the Omeka base.')
                    . ' ' . $translator->translate('The identifier should be set and unique in all records (collections, items, files).')
                    . ' ' . $translator->translate('This is generally the Dublin Core : Identifier or a specific element.'),
                'value_options' => $values,
            ],
            'attributes' => [
                'required' => true,
                'value' => $identifierField,
            ],
        ]);

        $this->add([
            'type' => 'Select',
            'name' => 'action',
            'options' => [
                'label' => $translator->translate('Action'),
                'info' => $translator->translate('The action defines how records and metadara are processed.')
                    . ' ' . $translator->translate('The record can be created, updated, deleted or skipped.')
                    . ' ' . $translator->translate('The metadata of an existing record can be updated, appended or replaced.'),
                'value_options' => [
                    '' => $translator->translate('No default action'),
                    \ArchiveFolder\Importer::ACTION_UPDATE_ELSE_CREATE
                        => $translator->translate('Update the record if it exists, else create one'),
                    \ArchiveFolder\Importer::ACTION_CREATE
                        => $translator->translate('Create a new record'),
                    \ArchiveFolder\Importer::ACTION_UPDATE
                        => $translator->translate('Update values of specific fields'),
                    \ArchiveFolder\Importer::ACTION_ADD
                        => $translator->translate('Add values to specific fields'),
                    \ArchiveFolder\Importer::ACTION_REPLACE
                        => $translator->translate('Replace values of all fields'),
                    \ArchiveFolder\Importer::ACTION_DELETE
                        => $translator->translate('Delete the record'),
                    \ArchiveFolder\Importer::ACTION_SKIP
                        => $translator->translate('Skip process of the record'),
                ],
            ],
        ]);

        $sharedEventManager->setIdentifiers('ArchiveFolder');
        $sharedEventManager->trigger('add_parameters');
    }

    /**
     * Return the list of available filtered values with a description.
     *
     * @return array Associative array of oai identifiers.
     */
    protected function getMappings()
    {
        $sharedEventManager = $this->getServiceLocator()->get('EventManager');
        $sharedEventManager->setIdentifiers('ArchiveFolder');
        $responses = $sharedEventManager->trigger('mappings');

        $mappings = [];
        foreach ($responses as $response) {
            foreach ($response as $key => $value) {
                if (class_exists($value['class'])) {
                    $mappings[$key] = $value['description'];
                }
            }
        }

        return $mappings;
    }
}
