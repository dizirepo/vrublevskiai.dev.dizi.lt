<?php

namespace ArchiveFolder\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * The ArchiveFolder Ajax controller class.
 *
 * @package ArchiveFolder
 */
class AjaxController extends AbstractActionController
{
    /**
     * Handle AJAX requests to delete a record.
     */
    public function deleteAction()
    {
        if (!$this->_checkAjax('delete')) {
            return;
        }

        $serviceLocator = $this->getServiceLocator();
        $api = $serviceLocator->get('Omeka\ApiManager');

        // Handle action.
        try {
            $id = (integer) $this->params('id');
            $response = $api->read('archivefolder_folders', $id);
            $folder = $response->getContent();
            if (!$folder) {
                $this->getResponse()->setStatusCode(400);
                return;
            }
            $api->delete('archivefolder_folders', $id);
        } catch (Exception $e) {
            $this->getResponse()->setStatusCode(500);
        }
    }

    /**
     * Check AJAX requests.
     *
     * 400 Bad Request
     * 403 Forbidden
     * 500 Internal Server Error
     *
     * @param string $action
     */
    protected function _checkAjax($action)
    {
        // Only allow AJAX requests.
        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            $this->getResponse()->setStatusCode(403);
            return false;
        }

        // Allow only valid calls.
        if ($this->params('controller') != 'ajax'
            || $this->params('action') != $action)
        {
            $this->getResponse()->setStatusCode(400);
            return false;
        }

        $acl = $this->getServiceLocator()->get('Omeka\Acl');

        // Allow only allowed users.
        if (!$acl->isAllowed('ArchiveFolder\Controller\Index', $action)) {
            $this->getResponse()->setStatusCode(403);
            return false;
        }

        return true;
    }
}
