<?php

namespace ArchiveFolder\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use ArchiveFolder\Form\AddForm;

/**
 * Controller for Achive Folder admin pages.
 *
 * @package ArchiveFolder
 */
class IndexController extends AbstractActionController
{
    /**
     * The number of records to browse per page.
     *
     * @var string
     */
    protected $_browseRecordsPerPage = 100;

    /**
     * Retrieve and render a set of records for the controller's model.
     */
    public function browseAction()
    {
        $serviceLocator = $this->getServiceLocator();

        $this->setBrowseDefaults('modified');
        $response = $this->api()->search('archivefolder_folders', $this->params()->fromQuery());
        $this->paginator($response->getTotalResults(), $this->params()->fromQuery('page'));

        $folders = $response->getContent();

        $view = new ViewModel;
        $view->setVariable('folders', $folders);
        $view->setVariable('paginator', $serviceLocator->get('Omeka\Paginator'));
        return $view;
    }

    public function addAction()
    {
        $serviceLocator = $this->getServiceLocator();
        $settings = $serviceLocator->get('Omeka\Settings');

        $form = new AddForm($this->getServiceLocator());
        $form->setAttribute('id', 'add-archivefolder-folder');

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if ($form->isValid()) {
                $response = $this->api()->create('archivefolder_folders', $data);
                if ($response->isError()) {
                    $errors = $response->getErrors();
                    $form->setMessages($errors);
                    $this->messenger()->addErrors($errors);
                } else {
                    $settings->set('archive_folder_identifier_field', $data['identifier_field']);
                    $this->messenger()->addSuccess($this->translate('Folder created'));
                    return $this->redirect()->toRoute('archivefolder_index');
                }
            } else {
                foreach ($form->getMessages() as $name => $message) {
                    $this->messenger()->addError(reset($message));
                }
                $this->messenger()->addError($this->translate('There was an error during validation'));
            }
        }

        $view = new ViewModel;
        $view->setVariable('form', $form);
        return $view;
    }

    public function stopAction()
    {
        $id = $this->params()->fromQuery('id');
        $response = $this->api()->read('archivefolder_folders', $id);
        $folder = $response->getContent();
        if (empty($folder)) {
            $message = $this->translate('Folder #%d does not exist.');
            $message = sprintf($message, $id);
            $this->messenger()->addError($message);
            return $this->redirect()->toRoute('archivefolder_index');
        }

        $folder->setStatus(ArchiveFolder\Folder::STATUS_STOPPED);
        $message = $this->translate('Process has been stopped.');
        $folder->addMessage($message);

        return $this->redirect()->toRoute('archivefolder_index');
    }

    public function resetStatusAction()
    {
        $folder = $this->_db->findById();
        if (empty($folder)) {
            $message = __('Folder #%d does not exist.', $this->_getParam('id'));
            $this->_helper->flashMessenger($message, 'error');
            return $this->_helper->redirector->goto('browse');
        }

        $folder->setStatus(ArchiveFolder_Folder::STATUS_RESET);
        $folder->save();

        $this->_helper->redirector->goto('browse');
    }

    /**
     * Check change in a folder.
     */
    public function checkAction()
    {
        $result = $this->_launchJob(ArchiveFolder_Builder::TYPE_CHECK);
        $this->_helper->redirector->goto('browse');
    }

    /**
     * Process a folder.
     */
    public function processAction()
    {
        $result = $this->_launchJob(ArchiveFolder_Builder::TYPE_UPDATE);
        $this->_helper->redirector->goto('browse');
    }

    /**
     * Batch editing of records.
     *
     * @return void
     */
    public function batchEditAction()
    {
        $folderIds = $this->_getParam('folders');
        if (empty($folderIds)) {
            $this->_helper->flashMessenger(__('You must choose some folders to batch edit.'), 'error');
        }
        // Normal process.
        else {
            // Action can be Check (default) or Update.
            $action = $this->getParam('submit-batch-process')
                ? ArchiveFolder_Builder::TYPE_UPDATE
                : ArchiveFolder_Builder::TYPE_CHECK;

            foreach ($folderIds as $folderId) {
                $result = $this->_launchJob(ArchiveFolder_Builder::TYPE_UPDATE, $folderId);
            }
        }

        $this->_helper->redirector->goto('browse');
    }

    public function logsAction()
    {
        $db = $this->_helper->db;
        $archiveFolder = $db->findById();

        $this->view->archiveFolder = $archiveFolder;
    }

    /**
     * Launch a process on a record.
     *
     * @param string $processType
     * @param integer $recordId
     * @return boolean Success or failure.
     */
    protected function _launchJob($processType = null, $recordId = 0)
    {
        $id = (integer) ($recordId ?: $this->_getParam('id'));

        $folder = $this->_db->find($id);
        if (empty($folder)) {
            $message = __('Folder # %d does not exist.', $id);
            $this->_helper->flashMessenger($message, 'error');
            return false;
        }

        if (in_array($folder->status, array(
                ArchiveFolder_Folder::STATUS_QUEUED,
                ArchiveFolder_Folder::STATUS_PROGRESS,
            ))) {
            return true;
        }

        $folder->setStatus(ArchiveFolder_Folder::STATUS_QUEUED);
        $folder->save();

        $options = array(
            'folderId' => $folder->id,
            'processType' => $processType,
        );

        $jobDispatcher = Zend_Registry::get('bootstrap')->getResource('jobs');
        $jobDispatcher->setQueueName(ArchiveFolder_UpdateJob::QUEUE_NAME);

        // Short dispatcher if user wants it.
        if (get_option('archive_folder_short_dispatcher')) {
            try {
                $jobDispatcher->send('ArchiveFolder_UpdateJob', $options);
            } catch (Exception $e) {
                $message = __('Error when processing folder.');
                $folder->setStatus(ArchiveFolder_Folder::STATUS_ERROR);
                $folder->addMessage($message, ArchiveFolder_Folder::MESSAGE_CODE_ERROR);
                _log('[ArchiveFolder] ' . __('Folder "%s" (#%d): %s',
                    $folder->uri, $folder->id, $message), Zend_Log::ERR);
                $this->_helper->flashMessenger($message, 'error');
                return false;
            }

            $message = __('Folder "%s" has been updated.', $folder->uri);
            $this->_helper->flashMessenger($message, 'success');
            return true;
        }

        // Normal dispatcher for long processes.
        $jobDispatcher->sendLongRunning('ArchiveFolder_UpdateJob', $options);
        $message = __('Folder "%s" is being updated.', $folder->uri)
            . ' ' . __('This may take a while. Please check below for status.');
        $this->_helper->flashMessenger($message, 'success');
        return true;
    }

    protected function _getDeleteConfirmMessage($record)
    {
        return __('When a folder is removed, the static repository file and the imported collections, items and files are not deleted from Omeka.');
    }

    /**
     * Return the number of records to display per page.
     *
     * @return integer|null
     */
    protected function _getBrowseRecordsPerPage($pluralName = null)
    {
        return $this->_browseRecordsPerPage;
    }
}
