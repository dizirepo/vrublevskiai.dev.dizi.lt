<?php

return [
    'view_manager' => [
        'template_path_stack' => [
            OMEKA_PATH.'/modules/MyModule/view',
        ],
    ],
    'controllers' => [
        'invokables' => [
            'MyModule\Controller\Admin\Index' => 'MyModule\Controller\Admin\IndexController',
        ],
    ],
];