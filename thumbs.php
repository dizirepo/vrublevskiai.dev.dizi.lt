<?php
createThumbs("files/original/","files/square/",200,200);
createThumbs("files/original/","files/medium/",200,0);
createThumbs("files/original/","files/large/",800,0);

function createThumbs( $pathToImages, $pathToThumbs, $maxWidth, $maxHeight ) 
{

set_time_limit(0);
ini_set('max_execution_time', 3000);

  // open the directory
  $dir = opendir( $pathToImages );
  // loop through it, looking for any/all JPG files:
  $i='1';
  while (false !== ($fname = readdir( $dir ))) {
    // parse path for the extension
	$img=null;
	
    $info = pathinfo($pathToImages . $fname);
    // continue only if this is a JPEG image

        $source_file_name = basename($fname);
        $source_image_type = substr($source_file_name, -3, 3);

		if(!empty($info['extension'])){
        switch(strtolower($info['extension'])){
            case 'gif':
                $img = imagecreatefromgif("{$pathToImages}{$fname}");
                break;
            case 'jpg':
                $img = imagecreatefromjpeg("{$pathToImages}{$fname}");
                break;
            case 'png':
                $img = imagecreatefrompng("{$pathToImages}{$fname}");
                break;
            case 'webp':
                $img = imagecreatefromwebp("{$pathToImages}{$fname}");
                break;
            default:
        }
		}
if(!empty($img)){
	  $imgsize = getimagesize($pathToImages . $fname);
    $width = $imgsize[0];
    $height = $imgsize[1];
	$max_width = $maxWidth;
	$max_height = $maxHeight;
	  if($max_height==0){
		$max_height = $height * ($max_width/$width);
	  }
	  if($max_width>$width){
		$max_width=$width;
	  }
	  if($max_height>$height){
		$max_height=$height;
	  }
	  
	$dst_img = imagecreatetruecolor($max_width, $max_height);
    $src_img = $img;
     
    $width_new = $height * $max_width / $max_height;
    $height_new = $width * $max_height / $max_width;
    //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
    if($width_new > $width){
        //cut point by height
        $h_point = (($height - $height_new) / 2);
        //copy image
        imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
    }else{
        //cut point by width
        $w_point = (($width - $width_new) / 2);
        imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
    }
     
    switch(strtolower($info['extension']))
    {
        case 'jpg':
            imagejpeg($dst_img, "{$pathToThumbs}{$fname}", 100);
            break;

        case 'gif':
            imagegif($dst_img, "{$pathToThumbs}{$fname}");
            break;

        case 'png':
            imagepng($dst_img,"{$pathToThumbs}{$fname}", 0);
            break;    
    }
 
    if($dst_img)imagedestroy($dst_img);
    if($src_img)imagedestroy($src_img);
	
	
	
      echo "$i : Creating thumbnail for $fname <br />";

 
    $i++;
}
      }
  // close the directory
  closedir( $dir );
}